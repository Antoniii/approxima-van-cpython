![](https://gitlab.com/Antoniii/approxima-van-cpython/-/raw/main/Figure_1.png)

## Задание состоит в построении графика интерполируемой и интерполирующей функции. Дана функция sin(x)*(0.25)+100, интервал от 50 до 60-ти, количество узлов 15.

![](https://gitlab.com/Antoniii/approxima-van-cpython/-/raw/main/approx.PNG)

![](https://gitlab.com/Antoniii/approxima-van-cpython/-/raw/main/мнк.PNG)

![](https://gitlab.com/Antoniii/approxima-van-cpython/-/raw/main/1.PNG)

![](https://gitlab.com/Antoniii/approxima-van-cpython/-/raw/main/lag.PNG)

![](https://gitlab.com/Antoniii/approxima-van-cpython/-/raw/main/example.PNG)

