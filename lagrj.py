import numpy as np
import matplotlib.pyplot as plt
import random


def phi(x):
    return sin(x)*(0.25)+100

x  = [50,60] 
a = 0
while a < 13 :
    random_number = random.uniform(0, 10) + 50
    while random_number in x : 
        random_number = random.uniform(0, 10) + 50
    x.append(random_number)
    a = a + 1
    x.sort()
print(x)
 
 
 
for k in range(len(x)):
    y=np.sin(x)*(0.25)+100
   
def poly(x, y, t):
    z = 0
    for i in range(len(x)):
        c1 = 1
        c2 = 1
        for j in range(len(x)):
            if i == j:
                c1 = c1 * 1
                c2 = c2 * 1
            else:
                c1 = c1 * (t - x[j])
                c2 = c2 * (x[i] - x[j])
        z = z + y[i] * c1 / c2
    return z
 
 
xnew = np.linspace(np.min(x), np.max(x), 100)
ynew = [poly(x, y, j) for j in xnew]
plt.plot(x,y)
plt.plot(x,y,'o',xnew,ynew)
plt.grid(True)
plt.show()